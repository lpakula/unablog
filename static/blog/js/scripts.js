$(document).ready(function(){

    $('#add_blog').ajaxBootstrapLink({
        modal: '#blog_modal',
        afterSuccess: function(){
            $('#blog_form_add').ajaxBootstrapForm({
                modal: true,
                afterSuccess:function(){
                    reload_blog_list();
                }
            });
        }
    });

    $(document).on('click', '.edit-blog', function() {
        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            success: function(data){
                $('body').append(data);
                   var modal = $('#blog_modal');
                   modal.modal();
                   modal.on('hidden.bs.modal', function (e) {
                       $(this).remove();
                   });

                $('#blog_form_add').ajaxBootstrapForm({
                    modal: true,
                    afterSuccess:function(){
                        reload_blog_list();
                    }
                });
            }
        });
        return false;
    });

    $(document).on('click', '.delete-blog', function() {
        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            success: function(data){
                $('body').append(data);
                var modal = $('#blog_modal_delete');
                modal.modal();
                modal.on('hidden.bs.modal', function (e) {
                    $(this).remove();
                });

                $('#blog_form_delete').ajaxBootstrapForm({
                    modal: true,
                    afterSuccess:function(){
                        reload_blog_list();
                    }
                });
            }
        });
        return false;
    });

});

function reload_blog_list(){
    // TODO: improve
    // set timeout to temporary fix content reloading issue,
    // apparently google put database update request in the queue instead of
    // save the record straight away
    setTimeout(
        function(){
            $('#blog').css('opacity', 0.8).load(
            '/blog/ajax/ > *', function (){
                $(this).css('opacity', 1);
        })
    }, 300);
}
