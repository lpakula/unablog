(function($){
    $.fn.extend({
        ajaxBootstrapForm: function(options) {
            var defaults = {
                beforeSubmit: function(data){},
                afterSuccess: function(data){},
                afterFail: function(data){},
                afterError: function(data){},
                afterTimeout: function(data){},
                modal: false,
                clearInput: false,
                redirect: false,
                timeout: 0
            };

            var options = $.extend(defaults, options);
            return this.each(function() {
                var _options = options;
                var form = $(this);
                // container to check offset against for messages
                if($('#main-content').length){
                    var main_content = $('#main-content > .wrapper');
                }else{
                    var main_content = $('body');
                }

                $(form).bind('submit', function () {
                    $.ajax({
                        data: $(this).serialize(),
                        type: $(this).attr('method'),
                        url: $(this).attr('action'),
                        success: processJson,
                        beforeSend: beforeSend,
                        error: afterError,
                        timeout: _options.timeout
                    });
                    return false;
                });


                function processJson(data){
                    _reset_form();
                    _empty_messages();

                    if(data.status == 'success'){
                        success(data);
                    }else if(data.status == 'pending'){
                        pending(data);
                    }else if(data.status == 'fail'){
                        fail(data);
                    }

                    if(data.messages){
                        _add_message(data);
                    }
                }

                function success(data){
                    if(_options.clearInput){
                        $('input:not(:hidden, [type=submit], [type=reset]), textarea, select', form).val('');
                        $('textarea').each(function(index){
                            var id = $(this).attr('id');
                            try {
                                if(tinymce.get(id)){
                                    tinymce.get(id).setContent('');
                                }
                            }
                            catch(err) {
                            }
                        });
                        if(jQuery().select2) {
                            $('select', form).select2('val', 0);
                        }
                    }

                    _enable_submit();
                    if(_options.modal){
                        _remove_modal();
                    }
                    _options.afterSuccess(data);

                    if(_options.redirect){
                        window.location = $(form).attr('data-redirect');
                    }
                }

                function pending(data){
                    _enable_submit();
                }

                function fail(data){
                    $.each(data.errors, function(fieldname ,message){
                        var id = '#id_'+fieldname;
                        // support widgets
                        if(!$(id).length){
                            var first_element = $(id+'_0');
                            if(first_element.length){
                                size = $(first_element).siblings('.form-control').length;
                                id = $(id+'_'+size)
                            }
                        }
                        $(id).after("<span class='help-block'>"+message+"</span>");
                        $(id).parents('.form-group').addClass('has-error');

                        if(fieldname == '__all__'){
                            data = {
                                "messages":[
                                    {"type":"danger", "text":message}
                                ]
                            }
                            _add_message(data)
                        }
                    });

                    var anchor = $('.has-error',form).first();
                    if(anchor.length > 0 && !data.message && !_options.modal){
                        $('html,body').animate({scrollTop: anchor.offset().top - main_content.offset().top -10 },'fast');
                    }

                    _enable_submit();
                    _options.afterFail(data);
                }

                 function afterError(data){
                    _empty_messages();
                    _enable_submit();
                    if(data.statusText == 'timeout'){
                        _options.afterTimeout(data);
                    }else{
                        _options.afterError(data)
                    }
                 }

                function beforeSend(data){
                    $('.btn', form).blur().addClass('disabled').append(' <i class="fa fa-spinner fa-spin"></i>');
                    _options.beforeSubmit(data);
                }

                function _remove_modal(){
                    $(form).parents('.modal').modal('hide');
                }

                function _enable_submit(){
                    $('.btn', form).blur().removeClass('disabled').children('i').remove();
                }

                function _reset_form(){
                    $('.form-group', form).removeClass('has-error');
                    $('span.help-block', form).remove();
                }

                function _empty_messages(){
                    $('#messages').empty();
                }

                function _add_message(data){
                    var messages = $('#messages');
                    var container = $('')
                    $.each(data.messages, function(type, message){
                        var type = message['type'];
                        var text = message['text'];
                        var alert =
                            '<div class="alert alert-'+type+' fade in">' +
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                    '<span aria-hidden="true">&times;</span>' +
                                '</button>' +
                                text +
                            '</div> ';
                        messages.append(alert);
                    });

                    $('html,body').animate({scrollTop: messages.offset().top - main_content.offset().top -10 },'fast');
                }

            });
        }
    });

})(jQuery);
