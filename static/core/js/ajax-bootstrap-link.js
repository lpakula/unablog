
(function($){
    $.fn.extend({
        ajaxBootstrapLink: function(options) {
            var defaults = {
                type: 'GET',
                beforeClick: function(data){},
                afterSuccess: function(data, element){},
                modal: false
            };

            var _options = $.extend(defaults, options);

            var link = $(this);

            if (link.length > 0){
                var selector = '';
                if(link[0].id){
                    selector += '#' + link[0].id;
                }
                if(link[0].className){
                    selector += '.' + link[0].className.split(' ').join('.');
                }
                $(document).on('click', selector, function() {
                    link_element = $(this);
                    $.ajax({
                        type: _options.type,
                        url: $(this).attr('href'),
                        success: processJson,
                        beforeSend: beforeSend
                    });
                    return false;
                });
            }

            function processJson(data){
                $('#messages').empty();


                if(data.messages){
                   _add_message(data);
                }

                if (_options.modal){
                    $('body').append(data);
                    var modal = $(_options.modal);
                    modal.modal();
                    modal.on('hidden.bs.modal', function (e) {
                        $(this).remove();
                    })
                }
                _options.afterSuccess(data, link);
                _enable_link()
            }

            function _enable_link(){
                $(link_element).removeClass('disabled').children('i.fa-spin').remove();
            }

            function _add_message(data){
                $.each(data.messages, function(type, message){
                    var type = message['type'];
                    var text = message['text'];
                    var alert =
                        '<div class="alert alert-'+type+' fade in">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                '<span aria-hidden="true">&times;</span>' +
                            '</button>' +
                            text +
                        '</div> ';

                    $('#messages').append(alert);
                });
                $('html,body').animate({scrollTop: $('#messages').offset().top - 5},'fast');
            }


            function beforeSend(data){
                $(link_element).addClass('disabled').append(' <i class="fa fa-spinner fa-spin"></i>');
                _options.beforeClick(data);
            }
        }
    });

})(jQuery);
