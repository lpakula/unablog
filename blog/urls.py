# -*- coding: utf-8 -*-

from django.conf.urls import url, patterns

from .views import BlogListView, BlogCreateView, BlogUpdateView, \
    BlogDeleteView, BlogAjaxView


urlpatterns = patterns(
    '',
    url(r'^$', BlogListView.as_view(), name="list"),
    url(r'^add/$', BlogCreateView.as_view(), name="add"),
    url(r'^update/(?P<pk>\d+)/$', BlogUpdateView.as_view(), name="update"),
    url(r'^delete/(?P<pk>\d+)/$', BlogDeleteView.as_view(), name="delete"),
    url(r'^ajax/$', BlogAjaxView.as_view(), name="ajax"),
    )
