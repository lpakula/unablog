# -*- coding: utf-8 -*-

from django import forms

from crispy_forms.helper import FormHelper

from .models import Blog


class BlogForm(forms.ModelForm):
    """
    Blog form
    """

    def __init__(self, *args, **kwargs):
        super(BlogForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

    class Meta:
        model = Blog
