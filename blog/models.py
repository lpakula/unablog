# -*- coding: utf-8 -*-

from django.db import models


class Blog(models.Model):
    """
    Model store blog entries
    """

    title = models.CharField(max_length=30)  #:
    content = models.TextField()  #:
    created = models.DateTimeField(auto_now_add=True)  #:
    updated = models.DateTimeField(auto_now=True)  #:
