# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, \
    DeleteView, View
from django.shortcuts import render_to_response
from django.template.context import RequestContext

from braces.views import JSONResponseMixin

from core.mixins import AjaxBootstrapFormMixin, AnonymousCsrfMixin
from .forms import BlogForm
from .models import Blog


class BlogListView(ListView):
    """
    Blog list view
    """
    model = Blog
    context_object_name = 'blogs'
    template_name = 'blog/index.html'


class BlogCreateView(AnonymousCsrfMixin, AjaxBootstrapFormMixin, CreateView):
    """
    Create blog view
    """
    model = Blog
    form_class = BlogForm
    template_name = 'blog/modal_add.html'
    success_url = reverse_lazy('blog:list')

    def form_valid(self, form):
        """
        :return: status response along with corresponding message
        :rtype: json
        """
        super(BlogCreateView, self).form_valid(form)
        return self.render_json_response(
            {"status": "success", "messages": [
                {'type': 'success', 'text': 'Blog has been added.'}]}
        )


class BlogUpdateView(AnonymousCsrfMixin, AjaxBootstrapFormMixin, UpdateView):
    """
    Update blog view
    """
    model = Blog
    form_class = BlogForm
    template_name = 'blog/modal_update.html'
    success_url = reverse_lazy('blog:list')

    def form_valid(self, form):
        """
        :return: status response along with corresponding message
        :rtype: json
        """
        super(BlogUpdateView, self).form_valid(form)
        return self.render_json_response(
            {"status": "success", "messages": [
                {'type': 'success', 'text': 'Blog has been updated.'}]}
        )


class BlogDeleteView(AnonymousCsrfMixin, AjaxBootstrapFormMixin, DeleteView):
    """
    Delete blog view
    """
    model = Blog
    context_object_name = 'blog'
    template_name = 'blog/modal_delete.html'
    success_url = reverse_lazy('blog:list')

    def delete(self, *args, **kwargs):
        """
        :return: status response along with corresponding message
        :rtype: json
        """
        super(BlogDeleteView, self).delete(*args, **kwargs)
        return self.render_json_response(
            {"status": "success", "messages": [
                {'type': 'success', 'text': 'Blog entry has been deleted.'}]}
        )


class BlogAjaxView(AnonymousCsrfMixin, JSONResponseMixin, View):
    """
    Ajax view handle template rendering for ajax calls
    """

    def get(self, request, *args, **kwargs):
        """
        :param request: current request
        :return: rendered list of blogs
        :rtype: html
        """
        blogs = Blog.objects.all()
        return render_to_response(
            'blog/list.html', {'blogs': blogs},
            context_instance=RequestContext(request))

