# -*- coding: utf-8 -*-

from django.contrib import admin

from .models import Blog


class BlogAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'updated', 'created')

admin.site.register(Blog, BlogAdmin)
