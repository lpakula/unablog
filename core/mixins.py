# -*- coding: utf-8 -*-

from django.utils.decorators import method_decorator

from session_csrf import anonymous_csrf
from braces.views import JSONResponseMixin


class AjaxBootstrapFormMixin(JSONResponseMixin):
    """
    Mixin to handle invalid form json response
    """

    def form_invalid(self, form):
        """
        :param form: current form
        :return: status response along with list of form errors
        :rtype: json
        """
        return self.render_json_response(
            {"status": "fail", "errors": form.errors})


class AnonymousCsrfMixin(object):

    @method_decorator(anonymous_csrf)
    def dispatch(self, *args, **kwargs):
        return super(AnonymousCsrfMixin, self).dispatch(*args, **kwargs)
